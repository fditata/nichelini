<?php
function tm_structure_child_enqueue_scripts() {
  wp_register_style( 'structure-child-style', get_stylesheet_directory_uri() . '/style.css'  );
  wp_enqueue_style( 'structure-child-style' );
}
add_action( 'wp_enqueue_scripts', 'tm_structure_child_enqueue_scripts', 11);


/*►Adding custom js script
----------------------------------------------------------------*/
function wpb_adding_scripts() {
    wp_register_script( 'custom_script', get_stylesheet_directory_uri() . '/js/custom_script.js', array('jquery'),'1.1', true );
	
	wp_enqueue_script( 'custom_script' );
} 
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );


/*►Favicon
----------------------------------------------------------------*/
function add_my_favicon() {
   $favicon_path = get_stylesheet_directory_uri() . '/favicon.ico';

   echo '<link rel="shortcut icon" href="' . $favicon_path . '" />';
}

add_action( 'wp_head', 'add_my_favicon' ); //front end
add_action( 'admin_head', 'add_my_favicon' ); //admin end